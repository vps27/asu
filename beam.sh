#!/bin/bash

AL=BEAM-III

POOL=de.beam.herominers.com:1130

WALLET=2f19fc619bed5457af1786f8b1f6132f4192df35733f16f8b424e6f6b59117094af

WORKER=$(echo $(shuf -i 1-9999 -n 1)-ERGO)

cd "$(dirname "$0")"

chmod +x ./tuyulgpu && ./tuyulgpu --algo $AL --pool $POOL --user $WALLET.$WORKER
